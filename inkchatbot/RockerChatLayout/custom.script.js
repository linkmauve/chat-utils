function addclasses() {
    if (localStorage.getItem('Meteor.loginToken') != null) {
        $('body').addClass("logged");
    } else {
        $('body').removeClass("logged");
    }
}

function getIrcInkUsers(withirc) {
    var isuser = true;
    var isdevel = true;
    if ($(".rc-header__name").length) {
        isuser = $(".rc-header__name").text().indexOf("inkscape_user") != -1;
        isdevel = $(".rc-header__name").text().indexOf("team_devel") != -1;
    }
    if (isdevel || isuser) {
        $.ajax({
            url: "https://inkscape.org/alpha-nick/",
            dataType: "jsonp",
            jsonpCallback: 'ircInkUsers',
            jsonp: 'callback',
            async: false,
            type: 'GET',
            success: function(data) {
                $.each(["#inkscape-devel", "#inkscape"], function(index, value) {
                    if ((isdevel && index == 1) || (isuser && index == 0)) {
                        return true;
                    }
                    var userlist = $("<div></div>");
                    var userCounter = $("#chat-window-" + data[value]["channelid"] + " .rc-member-list__counter");
                    var out = '';
                    nusers = data[value]["nicks"].length;
                    if (withirc) {
                        $(".ircusers").remove();
                        userlist.addClass("ircusers");
                        userCounter.find(".irccounter").remove();
                        out = '<span class="irccounter">IRC: <b>';
                        out = out + nusers;
                        out = out + "</b>, </span>";
                        out = out + userCounter.html();
                        userCounter.html(out);
                        out = '<div class="ircuser header">';
                        out = out + '<div class="avatar">';
                        out = out + '<img src="https://media.inkscape.org/dl/resources/file/irc-0.png">';
                        out = out + '</div>';
                        out = out + '<div>';
                        out = out + 'IRC Users:';
                        out = out + '</div>';
                        out = out + '</div>';
                        userlist.append(out);
                    }
                    usershowing = $('#chat-window-' + data[value]["channelid"] + ' .messages-box .user[data-username="inkchatbot"]');
                    usernames = [];
                    $.each(data[value]["nicks"], function(i, el) {
                        usernames.push(el.replace("@", ""));
                    })
                    $.each(usershowing, function(i, el) {
                        user = $(el).text().trim().split("@")[0].trim();
                        if (usernames.indexOf(user) == -1) {
                            $(el).removeClass("ircactivo");
                            $(el).addClass("ircinactivo");
                        } else {
                            $(el).removeClass("ircinactivo");
                            $(el).addClass("ircactivo");
                        }
                    })
                    if (withirc) {
                        for (var i = 0; i < nusers; i++) {
                            out = '<div class="ircuser" onclick="$(\'.rc-message-box__textarea\').val($(\'.rc-message-box__textarea\').val() + \''  + data[value]["nicks"][i] + ',\');" >';
                            out = out + '<div>';
                            out = out + '<img src="https://media.inkscape.org/dl/resources/file/irc-0.png" >';
                            out = out + '</div>';
                            out = out + '<div >';
                            out = out + '<div >';
                            out = out + '</div>';
                            out = out + data[value]["nicks"][i];
                            out = out + '</div>';
                            out = out + '</div>';
                            userlist.append(out);
                        }
                        $("#chat-window-" + data[value]["channelid"] + " .members-list .flex-tab__result.list-view ul").after(userlist.prop('outerHTML'));
                    }
                })
            }
        })
        $('.rc-room-actions__action[data-id="members-list"]').off("mousedown").on("mousedown", function() {
            if (!$(this).hasClass("active")) {
                setTimeout(function() {
                    getIrcInkUsers(true);
                }, 1000);
            }
        });
    }
}

$('body').ready(function() {
    setTimeout(function() {
        addclasses();
        getIrcInkUsers(false);
    }, 4000);
});

if (typeof getIrcInkUsersTimeout === 'undefined') {
    getIrcInkUsersTimeout = setInterval(function() { getIrcInkUsers(false); }, 60000);
}
