#!/bin/bash
# cd /home/chat-utils/inkchatbot && export PYTHONHASHSEED=0 && bash ./inkchatbotCron.bash
dir=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P)
echo "$dir"
string=$(ps xw | grep "python3" |  grep "inkchatbotDaemon.py")
step="/home/chat-utils/inkchatbot/inkchatbotDaemon.py"
previous="/home/chat-utils/inkchatbot/inkchatbotDaemon.py"
array=($string)
for i in "${array[@]}"
do
    if [ $previous = $step ]
    then
        if echo $i | egrep -q '^[0-9]+$'; then
            kill $i
        fi
    fi
    previous=$i
done
mv $dir/log.txt $dir/log.prev.txt
nohup /usr/bin/python3 $dir/inkchatbotDaemon.py  > $dir/log.txt 2>&1 &
