#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, sys, socket, multiprocessing, urllib , shlex, json, cgi, requests, re, datetime, time, ssl
from http.server import HTTPServer, BaseHTTPRequestHandler

RocketIRCBotDIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(RocketIRCBotDIR))
from inkchatbotConfig  import * 
from inkchatbotToken  import *

ircsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ircsock.connect((server, ircport))
ircsock.settimeout(None)

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def log_message(self, format, *args):
        pass
    
    def do_HEAD(self):
        self._set_headers()
    
    def do_GET(self):
        res = ""
        with open('users.json') as json_file:  
            users = json.load(json_file)
        users[channel1]["nicks"] = sorted(users[channel1]["nicks"], key=str.lower)
        users[channel2]["nicks"] = sorted(users[channel2]["nicks"], key=str.lower)  
        res = json.dumps(users)
        self.send_response(200)
        contentype = "application/json"
        if jsonp == True:
            contentype = "application/javascript"
            res = jsonpFunction + "(" + res+ ")"
        self.send_header("Content-type", contentype)
        self.send_header("Content-length", len(res))
        self.end_headers()
        self.wfile.write(bytes(res, 'UTF-8'))

    def do_POST(self):
        self._set_headers()
        form = cgi.FieldStorage(
            fp=self.rfile,
            headers=self.headers,
            environ={'REQUEST_METHOD': 'POST'}
        )

        messageid = shlex.quote(form.getvalue("message_id"))
        if messageid is not None and form.getvalue("token") == integrationToken:
            headers = {
                'X-Auth-Token': rocketchatUserToken ,
                'X-User-Id': rocketchatUserId,
            }
            params = (
                ('msgId', messageid),
            )
            try:
                response = requests.get('https://chat.inkscape.org/api/v1/chat.getMessage', headers=headers, params=params, verify=False)
                response.raise_for_status()
            except requests.HTTPError as e:
                return
            finalmsg = response.json()
            user = form.getvalue("user")
            txt = "<" + '\x03' + ("%02d" % (hash(user) % 14 + 2)) + user + '\x03' + "> "
            takefinalmessage = finalmsg.get('message')
            if takefinalmessage is not None:
                if takefinalmessage.get('editedAt') is not None:
                    if rocketedit == False:
                        return
                    txt = txt + "[Edited] ";
                if takefinalmessage.get('attachments') is not None:
                    for attach in takefinalmessage.get('attachments'):
                        if attach is not None and \
                        attach.get('description') is not None and \
                        attach.get('title_link') is not None and \
                        attach.get('title') is not None:
                            txt = txt + attach.get('description') 
                            txt = txt + ", see on https://chat.inkscape.org/" 
                            txt = txt + attach.get('title_link') 
                            txt = txt + " (" + attach.get('title') + ") \r\n"
            if form.getvalue("text") is not None:
                counter = 0
                for line in form.getvalue("text").split("\r\n"):
                    for linen in line.split("\n"):
                        for liner in linen.split("\r"):
                            counter = counter + 1
                            if counter < 3:
                                txt = txt + liner + "\r\n";
                            elif counter == 3:
                                txt = txt + "https://chat.inkscape.org/channel/" + rocketchatChannelName.get(form.getvalue("channel")) + "?msg=" + messageid + "\r\n";
            txt = re.sub(r'(^| )@(\w\w+)( |$|. )', r'\1@ \2 \3', txt, 20, re.MULTILINE)
            try:
                for line in txt.split("\r\n"):
                    for linen in line.split("\n"):
                        for liner in linen.split("\r"):
                            sendmsg(liner, form.getvalue("channel"))
            except (BrokenPipeError, IOError):
                pass

def split_utf8(s , n):
    assert n >= 4
    start = 0
    lens = len(s)
    while start < lens:
        if lens - start <= n:
            yield s[start:]
            return
        end = start + n
        while '\x80' <= s[end] <= '\xBF':
            end -= 1
        assert end > start
        yield s[start:end]
        start = end

def connect():
    ircsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ircsock.connect((server, ircport))
    ircsock.settimeout(None)
    login()
    
def login():
    ircsock.send(bytes("USER "+ botnick +"  8 * :" + rocketBotName + "\r\n", 'UTF-8')) # user information
    time.sleep(30.0);
    ircsock.send(bytes("NICK "+ botnick +"\r\n", 'UTF-8'))
    joinchan(channel1)
    joinchan(channel2)

def joinchan(chan): # join channel(s).
    ircsock.send(bytes("JOIN "+ chan +"\r\n", 'UTF-8'))
    ircmsg = ""
    try:
        while ircmsg.find("End of /NAMES") == -1: 
            ircmsg = ircsock.recv(2048).decode('UTF-8')
            ircmsg = ircmsg.strip('\r\n')
            if ircmsg != "":
                print(bytes(ircmsg, 'UTF-8'))
                if ircmsg.find("You have not registered") != -1:
                    ircsock.send(bytes("PRIVMSG NickServ :identify "+ password +"\r\n", 'UTF-8'));
                if ircmsg.find("Connection timed out") != -1:
                    connect()
                if ircmsg.find("PING :") != -1:
                    ping()
                if ircmsg.find("rocketirc has disconnected") != -1:
                    connect()
        ircsock.send(bytes("PRIVMSG NickServ :identify "+ password +"\r\n", 'UTF-8'));
    except socket.error:
        connect()

def ping():
    ircsock.send(bytes("PONG :pingis\r\n", 'UTF-8'))

def sendmsg(msg, channel):
    #FIXME we remove 70 from 510 chars to remove the initail message with server
    for msgsplit in split_utf8(msg , 440 - len(botnick + " PRIVMSG "+ channel +" :rn")):#510 with starting message
        time.sleep(0.33); #Prevent excess of flood
        ircsock.send(bytes("PRIVMSG "+ channel +" :"+ msgsplit +"\r\n", 'UTF-8'))

def toIRC():
    # Create server
    rocketchatUserId = "" 
    rocketchatUserToken = "" 
    currentdate = time.strftime("%x")
    httpd = HTTPServer(("", serverport), SimpleHTTPRequestHandler)
    if ssl == True:
        httpd.socket = ssl.wrap_socket (httpd.socket, 
        keyfile = sslCertificateKey, 
        certfile = sslCertificate, server_side=True)
    print("Serving on:" + str(serverport) + "...")
    httpd.serve_forever()

def names(channel): # sends messages to the target.
    ircsock.send(bytes("NAMES "+ channel +"\r\n", 'UTF-8'))

def retoken():
    headers = {
        'Content-type' :'application/json; charset=UTF-8',
    }
    params = {'user': botnick,'password': password}
    try:
        response = requests.post("https://chat.inkscape.org/api/v1/login", headers=headers, json=params, verify=False)
        response.raise_for_status()
    except requests.HTTPError as e:
        time.sleep(5);
        retoken()
        return
    autenticate = response.json()
    data = autenticate.get('data')
    if data is not None:
        file = open("./inkchatbotToken.py","w") 
        file.write("rocketchatUserId = \"" + data.get('userId') + "\" \n") 
        file.write("rocketchatUserToken = \"" + data.get('authToken') + "\" \n") 
        file.close()
        rocketchatUserId = data.get('userId')
        rocketchatUserToken = data.get('authToken')

def toRocketChat():
    retoken()
    ircactivity = [0,0]
    regex = re.compile("(\x0F|\x16|\x04|\x03|\x11|\x1E|\x1F|\x1D|\x02)(?:\d{1,2}(?:,\d{1,2})?)?", re.UNICODE)
    while 1:
        if datetime.datetime.now().day % 2 == 0:
            retoken()
        try:
            ircmsg = ircsock.recv(2048).decode('UTF-8', "ignore")
        except SocketError as e:
            headers = {
                'X-Auth-Token': rocketchatUserToken ,
                'X-User-Id': rocketchatUserId,
                'Content-type' :'application/json; charset=UTF-8',
            }
            irclogo   = ircIconBasePath + ("irc-0.png")
            roomId    = rocketchatChannelId.get(channel1)
            params = { "roomId": roomId,"alias": "inkchatbot", "text": "Lost connection to IRC. If the connection isn't automatically re-established within 2 minutes, please contact an administrator.", "avatar": irclogo}
            try:
                response = requests.post('https://chat.inkscape.org/api/v1/chat.postMessage', headers=headers, json=params, verify=False)
                response.raise_for_status()
            except requests.HTTPError as e:
                connect()
                continue
            connect()
            continue
        ircmsg = ircmsg.strip('\n\r')
        if ircmsg.find("PING :") != -1:
            ping()
        
        if ircmsg.find(" 353 " + botnick) != -1:
            headers = {
                    'X-Auth-Token': rocketchatUserToken ,
                    'X-User-Id': rocketchatUserId,
                    'Content-type' :'application/json; charset=UTF-8',
                }
            with open('users.json') as json_file:  
                users = json.load(json_file)
            channel = channel1
            if ircmsg.find(botnick + " = " + channel2 + " :") != -1:
                channel = channel2
            
            users[channel]["channelid"] = rocketchatChannelId.get(channel)
            users[channel]["nicks"] = []
            for userspart in ircmsg.split("\r\n"):
                if userspart.find("366 " + botnick + " " + channel + " :") == -1:
                    if userspart.find(channel + " :") != -1:
                        users[channel]["nicks"] = users[channel]["nicks"] + userspart.split(channel + " :")[1].replace(botnick + " ","").split(" ")               
            with open('users.json', 'w') as outfile:  
                json.dump(users, outfile)
        
        if ircmsg.find("QUIT") != -1 or ircmsg.find("PART") != -1 or ircmsg.find("JOIN") != -1:
            names(channel1)
            names(channel2)
                           
        if ircmsg.find("PRIVMSG") != -1:
            name = ircmsg.split('!',1)[0][1:]
            if "NAMES" in name:
                namearray = name.split(":")
                name = namearray[len(namearray) - 1]
            irclogo   = ircIconBasePath + ("irc-%d.png" % (hash(name) % 14 + 2))
            messageroot = ircmsg.split('PRIVMSG',1)[0]
            messages = ircmsg.split(messageroot + 'PRIVMSG',9999)
            for message in messages:
                if message != "":
                    onchannel = message.split(':',1)[0].strip()
                    message   = message.split(':',1)[1].split("\r\n",1)[0]
                    message   = regex.sub("", message)
                    roomId    = rocketchatChannelId.get(onchannel)

                    if len(name) < 17:
                        if message.find('Hi ' + botnick) != -1:
                            sendmsg("Hello " + name + "!", onchannel)
                            continue
                        elif name == botnick:
                            continue

                    if message != "":
                        if message.find(botnick) != -1:
                            ircsock.send(bytes("PRIVMSG " + name + " :You should message the user on chat.inkscape.org by using an @ symbol and not talk to me, I'm just the messenger. His name is after <" + botnick + "> \r\n", 'UTF-8'))
                        lastmessage = ""
                        headers = {
                            'X-Auth-Token': rocketchatUserToken ,
                            'X-User-Id': rocketchatUserId,
                            'Content-type' :'application/json; charset=UTF-8',
                        }
                        params = ()
                        try:
                            response = requests.get("https://chat.inkscape.org/api/v1/channels.list", headers=headers, params=params, verify=False)
                            response.raise_for_status()
                        except requests.HTTPError as e:
                            continue
                        channels = response.json()
                        channelist = channels.get("channels")
                        if channelist is not None:
                            for currentchannel in channelist:
                                if currentchannel.get("_id") == roomId:
                                    if currentchannel.get("lastMessage").get("alias") == name:
                                        lastmessage = currentchannel.get("lastMessage").get("_id")
                                                           
                        if lastmessage != "":
                            params = ()
                            try:
                                response = requests.get('https://chat.inkscape.org/api/v1/chat.getMessage?msgId=' + lastmessage, headers=headers, params=params, verify=False)
                                response.raise_for_status()
                            except requests.HTTPError as e:
                                continue
                            messagecontent = response.json()
                            mesagetaken = messagecontent.get("message")
                            if mesagetaken is not None:
                                originalmessage = mesagetaken.get("msg")
                                if originalmessage is not None:
                                    params = {"roomId": roomId, "msgId": lastmessage, "text": originalmessage + "\r\n" + message}
                                    requests.post('https://chat.inkscape.org/api/v1/chat.update', headers=headers, json=params, verify=False)
                        else:
                            params = { "roomId": roomId,"alias": name, "text": message, "avatar": irclogo}
                            requests.post('https://chat.inkscape.org/api/v1/chat.postMessage', headers=headers, json=params, verify=False)
                    if name.lower() == adminName.lower() and message.rstrip() == exitCode:
                        sendmsg("oh...okay. :'(", onchannel)
                        ircsock.send(bytes("QUIT \r\n", 'UTF-8'))
                        return

login()

t1 = multiprocessing.Process(target=toRocketChat)
t2 = multiprocessing.Process(target=toIRC)
t1.start()
t2.start()
