#!/usr/bin/python3
# -*- coding: utf-8 -*-

#Server
server = "chat.freenode.net"
#IrcPort
ircport = 6667
#ServerPort
serverport = 9753
#Use JsonP // not tested false
jsonp = True
#JsonP function
jsonpFunction = "ircInkUsers"
#Use SSL writing to IRC // not tested true
ssl = False
sslCertificate = "/"
sslCertificateKey = "/"
#Dont show editings on RC
rocketedit = False 
#IRC channel 1
channel1 = "#inkscape-devel" 
#IRC channel 2
channel2= "#inkscape" 
#Admin user, currently can exit bot
adminName = "jabiertxof"
#Your bots nick. also is the user in rocketchat
botnick = "inkchatbot" 
#Message to exit bot
exitCode = "bye " + botnick 
#rocketChat bot code
rocketBotCode = "rocket.cat" 
#rocketChat bot name
rocketBotName = "inkchatbot-0.6" 
#Base path where icons of IRC chanels are stored
ircIconBasePath = "https://media.inkscape.org/dl/resources/file/" 
#rocketChat integration token
integrationToken = "XXX" 
#Password of the nick. also is the password in rocketchat
password = "XXX"; 
#RC channelNameToPost
rocketchatChannelName = {
    channel1:"team_devel",
    channel2:"inkscape_user"
} 
#RC channelIdToPost https://rocket.chat/docs/developer-guides/rest-api/channels/list/
rocketchatChannelId = {
    channel1:"S8QyyXE5DNPi83KuL",
    channel2:"DdSRYnE833uGFna8J"
}
#RC Hook Data
irc2rocketHook = {
    channel1: "https://chat.inkscape.org/hooks/XXX",
    channel2: "https://chat.inkscape.org/hooks/XXX",
    "users":"https://chat.inkscape.org/hooks/XXX"
}

